﻿var modalConfirm = function (callback) {

    function getSessionValue(key) {
        var result;
        $.ajax({
            url: "/Account/GetSessionValue",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ key: key }),
            contentType: 'application/json; charset=utf-8',
            async: false
        }).done(function (data) {
            result = data;
        });
        return result;
    }

    var user = getSessionValue("uid");

    $("#btn-randevu").on("click", function () {
        if (user != null) {
            location.href = "/randevu-olustur";
        } else {
            $("#mi-modal").modal('show');
        }
    });

    $("#btn-randevularim").on("click", function () {
        if (user != null) {
            location.href = "/randevularim";
        } else {
            $("#mi-modal").modal('show');
        }
    });



    $("#modal-btn-si").on("click", function () {
        callback(true);
        location.href = "/giris-yap";

    });

    $("#modal-btn-no").on("click", function () {
        callback(false);
        $("#mi-modal").modal('hide');
    });
};

modalConfirm(function (confirm) {
    if (confirm) {
        //Acciones si el usuario confirma
        $("#result").html("CONFIRMADO");
    } else {
        //Acciones si el usuario no confirma
        $("#result").html("NO CONFIRMADO");
    }
});

$(document).ready(function () {
    // ülke seçildiğinde bu fonksiyon çalışır
    $('#bolums').change(function () {
        // seçilen ülkenin id sini al
        var bolumName = $(this).val();
        // secilen ülkenin id sini kullanarak Konum controller
        // sınıfı içerisindeki Sehirler metoduna çağrıda bulunuyoruz.
        // bu metod dan dönen listeyi kullanarak .each fonksiyonu ile
        // sehirleri dolduruyoruz...
        if (bolumName != null && bolumName != '') {
            $.ajax({
                type: "post",
                url: 'Home/Ogretmenler',
                data: { bolum: bolumName },
                success: function (ogretmenler) {
                    if (ogretmenler != null && ogretmenler.length > 0) {
                        $.each(ogretmenler, function (index, ogretmen) {
                            $('#teachers').empty();
                            $('#teachers').append($('<option/>', {
                                value: ogretmen.id,
                                text: ogretmen.İsim
                            }));
                        });
                    } else {
                        $('#teachers').empty();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("errorrr");
                },
            });
        }
    });
});