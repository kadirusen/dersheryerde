﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace aractaders
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "ContactPage",
               url: "iletisim",
               defaults: new { controller = "Home", action = "Contact" }
           );
            routes.MapRoute(
               name: "AboutPage",
               url: "hakkimizda",
               defaults: new { controller = "Home", action = "About" }
           );
            routes.MapRoute(
               name: "TeachersPage",
               url: "ogretmenlerimiz",
               defaults: new { controller = "Home", action = "Teachers" }
           );
            routes.MapRoute(
                name: "TeachersDetail",
                url: "ogretmendetay/{id}",
                defaults: new { controller = "Home", action = "TeachersDetail", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AppointmentPage",
                url: "randevu-olustur",
                defaults: new { controller = "Home", action = "Appoinment" }
            );
            routes.MapRoute(
                name: "Login",
                url: "giris-yap",
                defaults: new { controller = "Account", action = "Login" }
            );
            routes.MapRoute(
                name: "Register",
                url: "uye-ol",
                defaults: new { controller = "Account", action = "Register" }
            );
            routes.MapRoute(
                name: "RegisterLanding",
                url: "uyelik-olustur",
                defaults: new { controller = "Account", action = "RegisterLanding" }
            );
            routes.MapRoute(
                name: "MyAppoinment",
                url: "randevularim",
                defaults: new { controller = "Home", action = "MyAppoinment" }
            );
            routes.MapRoute(
                name: "TeacherResource",
                url: "ogretmen-basvuru",
                defaults: new { controller = "Account", action = "TeacherResource" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
