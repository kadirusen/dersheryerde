﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using aractaders.Models;
using System.Globalization;

namespace aractaders.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            index model = new index();
            model = index.GetModel();

            return View(model);
        }

        public ActionResult About()
        {
            dcityEntities db = new dcityEntities();
            var model = db.Hakkimizda;

            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Appoinment()
        {
            dcityEntities db = new dcityEntities();
            var model = db.Ogretmenler.ToList();
            var bolumModel = db.Bolum.ToList();
            List<SelectListItem> teachers = new List<SelectListItem>();
            List<SelectListItem> bolums = new List<SelectListItem>();


            foreach (var item in model)
            {
                teachers.Add(
                    new SelectListItem {
                        Text = item.İsim + " (" + item.Bolum + ")",
                        Value = item.id.ToString()
                    });
            }
            foreach (var item in bolumModel)
            {
                bolums.Add(
                    new SelectListItem {
                        Text = item.Isim,
                        Value = item.Isim
                    });
            }
            ViewBag.Teachers = teachers;
            ViewBag.Bolums = bolums;

            return View(model);
        }

        [HttpPost]
        public ActionResult Appoinment(string teachers, string date, string hour, string mobile, string adress) {
            bool isValid = false;

            if (Session["uid"].ToString() != null && teachers != null && date != null && hour != ""  && mobile != "" && adress != "" )
            {
                using (dcityEntities entity = new dcityEntities())
                {
                    List<SelectListItem> _teachers = new List<SelectListItem>();
                    List<SelectListItem> _bolums = new List<SelectListItem>();

                    string dateHour = date + "-" + hour;
                    string[] _splitDate = dateHour.Split('-');
                    string _date = _splitDate[0].Trim();
                    string _hour = _splitDate[1].Trim() + "-" + _splitDate[2].Trim();
                    string _startHour = _splitDate[1].Trim();
                    string _endHour = _splitDate[2].Trim();

                    if (entity.Randevular.Any(x => x.tarih == _date))
                    {
                        var randevus = entity.Randevular.Where(x => x.tarih == _date).ToList();

                        if (randevus.Any(x => x.saat.Contains(_startHour) || x.saat.Contains(_endHour)))
                        {

                           foreach (var item in entity.Ogretmenler.ToList())
                            {
                                _teachers.Add(
                                     new SelectListItem
                                     {
                                         Text = item.İsim + " (" + item.Bolum + ")",
                                         Value = item.id.ToString()
                                     });
                            }

                            foreach (var item in entity.Bolum.ToList())
                            {
                                _bolums.Add(
                                    new SelectListItem
                                    {
                                        Text = item.Isim,
                                        Value = item.Isim
                                    });
                            }

                            ViewBag.IsValid = "Bu tarih ve saat uygun değil, lütfen tekrar deneyiniz..";
                            ViewBag.Teachers = _teachers;
                            ViewBag.Bolums = _bolums;

                            return View();
                        }
                    }

                    int ogrtId = Convert.ToInt32(teachers);
                    int ogretmenId = entity.Ogretmenler.Where(x => x.id == ogrtId).FirstOrDefault().id;
                    string ogretmenAd = entity.Ogretmenler.Where(x => x.id == ogrtId).FirstOrDefault().İsim;
                    string bolum = entity.Ogretmenler.Where(x => x.id == ogrtId).FirstOrDefault().Bolum;
                    int ogrenciId = Convert.ToInt32(Session["uid"].ToString());
                    string ogrenciAd = entity.Uyeler.Where(x => x.id == ogrenciId).FirstOrDefault().ad;
                    string ogrenciSoyad = entity.Uyeler.Where(x => x.id == ogrenciId).FirstOrDefault().soyad;
                    string ogrenciEmail = entity.Uyeler.Where(x => x.id == ogrenciId).FirstOrDefault().email;

                   
                    var newDate = DateTime.ParseExact(date.ToString(), "dd/M/yyyy",
                                                CultureInfo.InvariantCulture);


                    Randevular randevu = new Randevular();
                        randevu.ogrenci_id = ogrenciId;
                        randevu.ogretmen_id = ogretmenId;
                        randevu.ogrenci_adi = ogrenciAd + " " + ogrenciSoyad;
                        randevu.email = ogrenciEmail;
                        randevu.ogretmen_adi = ogretmenAd;
                        randevu.bolum = bolum;
                        randevu.tarih = _date;
                        randevu.saat = _hour;
                        randevu.randevu_tarih = DateTime.Today.Date;
                        randevu.ogrenci_tel = mobile;
                        randevu.adres = adress;
                        randevu.ETarih = DateTime.Now;
                        randevu.Onay = false;


                        entity.Randevular.Add(randevu);
                        entity.SaveChanges();

                     return View("AppoinmentSuccess");


                }
            }
            else
            {
                using (dcityEntities entity = new dcityEntities()) {

                    List<SelectListItem> _teachers = new List<SelectListItem>();
                    List<SelectListItem> _bolums = new List<SelectListItem>();

                    foreach (var item in entity.Ogretmenler.ToList())
                    {
                        _teachers.Add(
                             new SelectListItem
                             {
                                 Text = item.İsim + " (" + item.Bolum + ")",
                                 Value = item.id.ToString()
                             });
                    }

                    foreach (var item in entity.Bolum.ToList())
                    {
                        _bolums.Add(new SelectListItem {
                            Text = item.Isim,
                            Value = item.Isim


                        });
                    }

                    ViewBag.IsValid = "Lütfen tüm alanları doldurunuz..";
                    ViewBag.Teachers = _teachers;
                    ViewBag.Bolums = _bolums;

                }

                return View();
            }

        }

        public ActionResult Teachers()
        {
            dcityEntities db = new dcityEntities();
            var model = db.Ogretmenler;
            return View(model.Where(x => x.Onay == true));
        }

        public ActionResult TeachersDetail(int id) {

            dcityEntities db = new dcityEntities();
            var model = db.Ogretmenler.Where(y => y.id == id).ToList();
            var randevus = db.Randevular.Where(x => x.ogretmen_id == id).OrderByDescending(y => y.id).Take(5).ToList();

            ViewBag.Randevular = randevus;
            return View(model);
        }

        public ActionResult MyAppoinment() {
            if (Session["uid"] != null && Session["utype"] != null)
            {
                using (dcityEntities entity = new dcityEntities()) {

                    if (Session["utype"] == "student")
                    {
                        int uid = Convert.ToInt32(Session["uid"]);
                        var list = entity.Randevular.Where(x => x.ogrenci_id == uid).ToList();
                        return View(list);
                    }
                    else if (Session["utype"] == "teacher")
                    {
                        int uid = Convert.ToInt32(Session["uid"]);
                        var list = entity.Randevular.Where(x => x.ogretmen_id == uid).ToList();
                        return View(list);
                    }

                }
            }
            else
            {

            }


            return View();
        }

        public ActionResult Ogretmenler(string bolum) {
            List<Ogretmenler> ogretmenler = new List<Ogretmenler>();
            dcityEntities entity = new dcityEntities();
            var list = entity.Ogretmenler.ToList();

            foreach (var item in list)
            {
                if (item.Bolum == bolum)
                {
                    ogretmenler.Add(new Ogretmenler{
                          id = item.id,
                          İsim = item.İsim,
                          Bolum = item.Bolum
                    });
                }
            }

            return Json(ogretmenler);

        }
    }
}