﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using aractaders.Models;
using aractaders.Helpers;
using System.IO;

namespace aractaders.Controllers
{
    public class AccountController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult LoginSuccess()
        {
            return View();
        }

        public ActionResult Register() {
            return View();
        }

        public ActionResult RegisterLanding() {

            return View();
        }

        public ActionResult RegisterSuccess()
        {
            return View();
        }

        public ActionResult TeacherResource() {
            dcityEntities db = new dcityEntities();
            var model = db.Bolum.ToList();
            List<SelectListItem> bolumler = new List<SelectListItem>();

            foreach (var item in model)
            {
                bolumler.Add(
                    new SelectListItem {
                        Text = item.Isim,
                        Value = item.Isim
                    });
            }
            ViewBag.Bolumler = bolumler;

            return View();
        }
        public ActionResult TeacherResourceSuccess()
        {

            return View();
        }

        public ActionResult Logout() {

            Session["uid"] = null;
            Session["email"] = null;

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel uye) {

            if (ModelState.IsValid)
            {
                using (dcityEntities entity = new dcityEntities()) {
                    var uyeObj = entity.Uyeler.Where(x => x.email.Equals(uye.email) && x.sifre.Equals(uye.sifre)).FirstOrDefault();
                    var ogrtObj = entity.Ogretmenler.Where(x => x.Email.Equals(uye.email) && x.Sifre.Equals(uye.sifre)).FirstOrDefault();
                    if (uyeObj != null && uyeObj.Onay == true)
                    {
                        Session["uid"] = uyeObj.id.ToString();
                        Session["email"] = uyeObj.email.ToString();
                        Session["utype"] = "student";
                        Session["ad"] = uyeObj.ad.ToString() + " " + uyeObj.soyad.ToString();

                        return RedirectToAction("LoginSuccess");
                    }
                    else if (ogrtObj != null && ogrtObj.Onay == true)
                    {
                        Session["uid"] = ogrtObj.id.ToString();
                        Session["email"] = ogrtObj.Email.ToString();
                        Session["utype"] = "teacher";
                        Session["ad"] = ogrtObj.İsim.ToString();
                        return RedirectToAction("LoginSuccess");

                    }
                    else
                    {
                        ModelState.AddModelError("", "Lütfen tekrar deneyiniz..");

                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Lütfen tekrar deneyiniz..");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Register(Uyeler uye)
        {
            if (ModelState.IsValid)
            {
                using (dcityEntities entity = new dcityEntities())
                {
                    uye.Onay = true;
                    entity.Uyeler.Add(uye);
                    entity.SaveChanges();
                }
                return View("RegisterSuccess");
            }
            else
            {
                ModelState.AddModelError("", "Lütfen tüm alanları doldurunuz.");
            }
            return View("Register");
        }

        [HttpPost]
        public ActionResult TeacherResource(string isim, string bolumler, string ozgecmis, string tel, string email, string sifre) {

            if (isim != null && bolumler != null && ozgecmis != null && tel != null && email != null && sifre != null && Request.Files.Count > 0)
            {
                byte[] content;
                var file = Request.Files[0];
                var fileId = 0;
                var randFileName = GenericHelpers.RandomString(8);

                dcityEntities db = new dcityEntities();

                if (file != null && file.ContentLength > 0 && (file.FileName.Contains(".jpg") || file.FileName.Contains(".jpeg") || 
                    file.FileName.Contains(".png") || file.FileName.Contains(".JPG") || file.FileName.Contains(".JPEG") || file.FileName.Contains(".PNG")))
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        content = br.ReadBytes(file.ContentLength);
                    }
                    file.InputStream.Read(content, 0, file.ContentLength);
                    var _fileName = randFileName + "_" + Path.GetFileName(file.FileName);
                    db.FileRepository.Add(new FileRepository
                    {
                        FileName = _fileName,
                        FileBytes = content,
                        ModifiedDate = DateTime.Now
                    });

                    db.SaveChanges();

                    fileId = db.FileRepository.Where(x => x.FileName == _fileName).FirstOrDefault().FileID;

                }
                else
                {
                    var model = db.Bolum.ToList();
                    List<SelectListItem> _bolumler = new List<SelectListItem>();

                    foreach (var item in model)
                    {
                        _bolumler.Add(
                            new SelectListItem
                            {
                                Text = item.Isim,
                                Value = item.Isim
                            });
                    }
                    ViewBag.IsValid = "Dosya uzantısı .jpg yada .png olması gerekmektedir..";
                    ViewBag.Bolumler = _bolumler;
                    return View();
                }

                using (dcityEntities entity = new dcityEntities()){
                   
                    Ogretmenler ogretmen = new Ogretmenler();
                    ogretmen.İsim = isim;
                    ogretmen.Bolum = bolumler;
                    ogretmen.Ozgecmis = ozgecmis;
                    ogretmen.Tel = tel;
                    ogretmen.Email = email;
                    ogretmen.ResimId = fileId != 0 ? fileId : 0;
                    ogretmen.Sifre = sifre;
                    ogretmen.ETarih = DateTime.Now;
                    ogretmen.Onay = false;

                    entity.Ogretmenler.Add(ogretmen);
                    entity.SaveChanges();


                    return View("TeacherResourceSuccess");
                }

            }
            else
            {
                dcityEntities db = new dcityEntities();
                var model = db.Bolum.ToList();
                List<SelectListItem> _bolumler = new List<SelectListItem>();

                foreach (var item in model)
                {
                    _bolumler.Add(
                        new SelectListItem
                        {
                            Text = item.Isim,
                            Value = item.Isim
                        });
                }
                ViewBag.IsValid = "Lütfen tüm alanları doldurunuz..";
                ViewBag.Bolumler = _bolumler;
            }

            return View();
        }

        public JsonResult GetSessionValue(string key)
        {
            return Json(Session[key]);
        }

    }
}