﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace aractaders.Helpers
{
    public class GenericHelpers : Controller
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        //public byte[] Crop(FileUpload fileUpload, int width, int height)
        //{
        //    byte[] fileBytes = fileUpload.FileBytes;

        //    string fileName = fileUpload.FileName;

        //    try
        //    {
        //        System.Drawing.Image UploadedImage = System.Drawing.Image.FromStream(fileUpload.PostedFile.InputStream);

        //        float UploadedImageWidth = UploadedImage.PhysicalDimension.Width;

        //        float UploadedImageHeight = UploadedImage.PhysicalDimension.Height;

        //        if (UploadedImageWidth > width || UploadedImageHeight > height)
        //        {
        //            ImageX imageLogic = new ImageX();

        //            fileBytes = imageLogic.CropX(fileUpload.FileBytes, width, height, "", 80);
        //        }

        //    }
        //    catch { return new byte[0]; }

        //    return fileBytes;
        //}
    }
}