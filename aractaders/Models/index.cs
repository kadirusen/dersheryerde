﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aractaders.Models
{
    public class index
    {
        public List<Showcase> mansetTumu { get; set; }
        public List<BizKimiz> bizKimizTumu {get; set; }
        public List<Duyuru> duyuruTumu { get; set; }
        public List<Ogretmenler> ogretmenlerTumu { get; set; }

        public static index GetModel()
        {
            index model = new index();

            using (var indexContents = new dcityEntities())
            {
                var manset_list = indexContents.Showcase.OrderByDescending(x => x.ETarih).ToList();
                var bizKimiz_list = indexContents.BizKimiz.OrderByDescending(x => x.ETarih).ToList();
                var duyuru_list = indexContents.Duyuru.OrderByDescending(x => x.ETarih).ToList();
                var ogretmenler_list = indexContents.Ogretmenler.OrderByDescending(x => x.Bolum).ToList();

                var mansetListe = new List<Showcase>();
                var bizKimizListe = new List<BizKimiz>();
                var duyuruListe = new List<Duyuru>();
                var ogretmenlerListe = new List<Ogretmenler>();

                foreach (var item in manset_list.Where(x => x.Onay == true))
                {
                    Showcase manset = new Showcase();
                    manset.Baslik = item.Baslik;
                    manset.Aciklama = item.Aciklama;
                    manset.ETarih = item.ETarih;
                    manset.FileName = item.FileName;
                    manset.ResimId = item.ResimId;
                    manset.Onay = item.Onay;
                    manset.Sira = item.Sira;
                    manset.Url = item.Url;
                
                    manset.Id = (int)item.Id;
                    mansetListe.Add(manset);
                }

                model.mansetTumu = mansetListe.OrderByDescending(x => x.Sira).ToList();

                foreach (var item in bizKimiz_list.Where(y=> y.Onay == true))
                {
                    BizKimiz bizKimiz = new BizKimiz();
                    bizKimiz.Baslik = item.Baslik;
                    bizKimiz.Aciklama = item.Aciklama;
                    bizKimiz.ETarih = item.ETarih;
                    bizKimiz.Id = item.Id;

                    bizKimizListe.Add(bizKimiz);
                }

                model.bizKimizTumu = bizKimizListe;

                foreach (var item in duyuru_list.Where(y => y.Onay == true))
                {
                    Duyuru duyuru = new Duyuru();
                    duyuru.Baslik = item.Baslik;
                    duyuru.Aciklama = item.Aciklama;
                    duyuru.ETarih = item.ETarih;
                    duyuru.Id = item.Id;

                    duyuruListe.Add(duyuru);
                }

                model.duyuruTumu = duyuruListe;


                foreach (var item in ogretmenler_list.Where(z => z.Onay == true))
                {
                    Ogretmenler ogretmen = new Ogretmenler();
                    ogretmen.İsim = item.İsim;
                    ogretmen.Ozgecmis = item.Ozgecmis;
                    ogretmen.Bolum = item.Bolum;
                    ogretmen.Email = item.Email;
                    ogretmen.Tel = item.Tel;
                    ogretmen.id = item.id;
                    ogretmen.ResimId = item.ResimId;

                    ogretmenlerListe.Add(ogretmen);
                }

                model.ogretmenlerTumu = ogretmenlerListe;
            }


            return model;
        }
    }

}